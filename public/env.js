window.env = {
  "REACT_APP_BASE_URL_NG": "https://api.motion-s.com/platform",
  "REACT_APP_ANALYTICS": "https://api.motion-s.com/analytics/info/v1/fleets",
  "REACT_APP_ANALYTICS_RISK": "https://api.motion-s.com/analytics/risk",
  "REACT_APP_DRIVE_DASHBOARD": "drive/v1/dashboard"
};