import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import "./App.css";
import Dashboard from "./screens/Dashboard";
import HomeScreen from "./screens/HomeScreen";
import LoginScreen from "./screens/LoginScreen";
import Header from "./components/Header";
import Footer from "./components/Footer";

const App = () => {
  return (
    <Router>
      <Header />
      <Container>
        <Route path="/dashboard/fleet/:id" component={Dashboard} exact />
        <Route path="/login" component={LoginScreen} />

        <Route path="/" component={HomeScreen} exact />
      </Container>
      <Footer />
    </Router>
  );
};

export default App;
