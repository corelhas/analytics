import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { userLoginReducer } from "./reducers/userReducer";
import { getDriversReducer } from "./reducers/driverReducer";
import {
  getFleetsReducer,
  getFleetStatsReducer,
  getFleetByIdReducer,
} from "./reducers/fleetReducer";
import { getFleetRiskSummaryReducer } from "./reducers/analytics/fleet/fleetAnalyticsReducer";
import { getDeviceReducer } from "./reducers/devicesReducer";
import { getDriverDashboardReducer } from "./reducers/analytics/driver/driverDashboardReducer";

const reducer = combineReducers({
  userLogin: userLoginReducer,

  getDrivers: getDriversReducer,

  getFleets: getFleetsReducer,
  getFleetStats: getFleetStatsReducer,
  getFleetById: getFleetByIdReducer,

  getFleetRisk: getFleetRiskSummaryReducer,

  getDashboardDriver: getDriverDashboardReducer,

  getDevices: getDeviceReducer,
});

const userInfoFromStorage = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null;

const initialState = {
  userLogin: { userInfo: userInfoFromStorage },
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
