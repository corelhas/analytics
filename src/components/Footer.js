import React from "react";

const Footer = () => {
  return (
    <footer className="fdb-block footer-small bg-dark">
      <div className="container">
        <div className="row text-center align-items-center ">
          <div className="col">
            <p className="h5 mt-5 text-white">
              © Copyright 2021 - Motion-S S.A. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
