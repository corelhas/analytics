import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Row, Col, Button } from "react-bootstrap";
import { Layout, Menu } from "antd";
import Loader from "../components/Loader";
import {
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
// import Loader from "../components/Loader";
// import { getDriversInfo } from "../actions/driverActions";
// import { getFleetInfo, getInfoFleetsById } from "../actions/fleetActions";
// import { getDevicesInfo } from "../actions/devicesActions";
import { getRiskSummary } from "../actions/analytics/fleet/fleetAnalyticsActions";
import { getDriverDashboard } from "../actions/analytics/driver/driverDashboardActions";
import { ResponsiveLine } from "@nivo/line";
import { LinkContainer } from "react-router-bootstrap";
import EventsMap from "../components/EventsMap";

const { Content, Sider } = Layout;

const Dashboard = ({ match, history }) => {
  const [dataGraph, setDataGraph] = useState([]);
  const [unit, setUnit] = useState("");
  const [goodDriver, setGoodDriver] = useState("");
  const [badDriver, setBadDriver] = useState("");
  const [averageDriver, setAverageDriver] = useState("");

  const dispatch = useDispatch();

  const fleet_id = match.params.id;

  // const getDrivers = useSelector((state) => state.getDrivers);
  // const { loading, drivers } = getDrivers;

  // const getFleets = useSelector((state) => state.getFleets);
  // const { loadingFleet, fleets } = getFleets;

  // const getDevices = useSelector((state) => state.getDevices);
  // const { loadingDevice, devices } = getDevices;

  // const getFleetById = useSelector((state) => state.getFleetById);
  // const { loadingFleetsById, fleetInfo } = getFleetById;

  const getFleetRisk = useSelector((state) => state.getFleetRisk);
  const { loadingFleetRisk, fleetRisk } = getFleetRisk;

  const getDashboardDriver = useSelector((state) => state.getDashboardDriver);
  const { loadingDashboardDriver, driverDashboard } = getDashboardDriver;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    if (!userInfo) history.push("/login");

    const fetchData = async () => {
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.access_token}`,
        },
      };

      let response = await fetch(
        `${process.env.REACT_APP_ANALYTICS_RISK}/${process.env.REACT_APP_DRIVE_DASHBOARD}?fleet_id=${fleet_id}`,
        config
      );

      if (response.status === 404 || response.status === 401) {
        return <h1>Need to handle this error</h1>;
      }

      let dataGraph = await response.json();
      setDataGraph(dataGraph);

      let dataForChart = dataGraph.evolution.drive.map((chart) => {
        return { x: chart.x, y: chart.y };
      });

      let unit = dataGraph.evolution.unit;

      let goodDriver = dataGraph.count.population.good;
      let badDriver = dataGraph.count.population.bad;
      let averageDriver = dataGraph.count.population.average;

      let data = [
        {
          id: "drive",
          color: "hsl(160, 70%, 50%)",
          data: dataForChart,
        },
      ];

      setGoodDriver(goodDriver);
      setBadDriver(badDriver);
      setAverageDriver(averageDriver);
      setUnit(unit);
      setDataGraph(data);
    };

    fetchData();
    dispatch(getDriverDashboard(fleet_id));

    // dispatch(getDriversInfo());
    // dispatch(getFleetInfo());
    // dispatch(getDevicesInfo());
    // dispatch(getInfoFleetsById(fleet_id));
    dispatch(getRiskSummary(fleet_id));
  }, [history, userInfo]);

  // console.log(driverDashboard.count.population.good);

  let data = JSON.parse(JSON.stringify(dataGraph));

  return (
    <Layout>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
      >
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={["2"]}
          style={{ height: "100%" }}
        >
          <Menu.Item key="1" icon={<UserOutlined />}>
            Fleet Overview
          </Menu.Item>
          <Menu.Item key="2" icon={<VideoCameraOutlined />}>
            Risk Assessment
          </Menu.Item>
          <Menu.Item key="3" icon={<UploadOutlined />}>
            Contextual Data
          </Menu.Item>
          <Menu.Item key="4" icon={<UploadOutlined />}>
            EV Assessment
          </Menu.Item>
          <Menu.Item key="5" icon={<UploadOutlined />}>
            ECO Assessment
          </Menu.Item>
          <Menu.Item key="6" icon={<UploadOutlined />}>
            Vehicle Health
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout" style={{ marginLeft: 100 }}>
        <Content style={{ padding: "0 0px", marginTop: 2 }}>
          <div
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            <LinkContainer to={"/"} style={{ marginLeft: 100 }}>
              <Button variant="dark" className="btn-sm">
                Back
              </Button>
            </LinkContainer>
            <div style={{ height: 300 }}>
              {loadingDashboardDriver ? (
                <Loader />
              ) : driverDashboard ? (
                <ResponsiveLine
                  data={data}
                  margin={{ top: 50, right: 110, bottom: 50, left: 100 }}
                  xScale={{ type: "point" }}
                  yScale={{
                    type: "linear",
                    min: 0,
                    max: 100,
                    stacked: true,
                    reverse: false,
                  }}
                  yFormat=" >-.2f"
                  curve="natural"
                  axisTop={null}
                  axisRight={null}
                  axisBottom={{
                    orient: "bottom",
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: unit,
                    legendOffset: 36,
                    legendPosition: "middle",
                  }}
                  axisLeft={{
                    orient: "left",
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: "Drive Score",
                    legendOffset: -40,
                    legendPosition: "middle",
                  }}
                  enableGridX={false}
                  enableGridY={false}
                  colors={{ scheme: "nivo" }}
                  lineWidth={5}
                  pointSize={8}
                  pointColor={{ theme: "background" }}
                  pointBorderWidth={2}
                  pointBorderColor={{ from: "serieColor" }}
                  pointLabelYOffset={-12}
                  enableArea={true}
                  useMesh={true}
                  legends={[
                    {
                      anchor: "bottom-right",
                      direction: "column",
                      justify: false,
                      translateX: 100,
                      translateY: 0,
                      itemsSpacing: 0,
                      itemDirection: "left-to-right",
                      itemWidth: 80,
                      itemHeight: 20,
                      itemOpacity: 0.75,
                      symbolSize: 12,
                      symbolShape: "circle",
                      symbolBorderColor: "rgba(0, 0, 0, .5)",
                      effects: [
                        {
                          on: "hover",
                          style: {
                            itemBackground: "rgba(0, 0, 0, .03)",
                            itemOpacity: 1,
                          },
                        },
                      ],
                    },
                  ]}
                />
              ) : (
                <Loader />
              )}
            </div>

            <Row xs={1} md={3} className="g-3">
              <Col sm>
                <Card style={{ width: "10rem" }} border="success">
                  {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                  <Card.Body
                    style={{ fontSize: "5rem" }}
                    className="text-center"
                  >
                    {loadingDashboardDriver ? (
                      <Loader />
                    ) : driverDashboard ? (
                      <h1>{goodDriver}</h1>
                    ) : (
                      <Loader />
                      // <h1>There are no information about Safety Score</h1>
                    )}
                  </Card.Body>
                </Card>
              </Col>
              <Col sm>
                <Card style={{ width: "10rem" }} border="danger">
                  {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                  <Card.Body
                    style={{ fontSize: "5rem" }}
                    className="text-center"
                  >
                    {loadingDashboardDriver ? (
                      <Loader />
                    ) : driverDashboard ? (
                      <h1>{averageDriver}</h1>
                    ) : (
                      <Loader />
                    )}
                  </Card.Body>
                </Card>
              </Col>
              <Col sm>
                <Card style={{ width: "10rem" }} border="warning">
                  <Card.Body
                    style={{ fontSize: "5rem" }}
                    className="text-center"
                  >
                    {loadingDashboardDriver ? (
                      <Loader />
                    ) : driverDashboard ? (
                      <h1>{badDriver}</h1>
                    ) : (
                      <Loader />
                    )}
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            <EventsMap />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default Dashboard;
