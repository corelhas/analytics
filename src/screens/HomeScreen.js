import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { Table, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Table, Button } from "react-bootstrap";
import { getFleetInfo, getStatsFleet } from "../actions/fleetActions";
import Loader from "../components/Loader";
// import { Popover } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
// import { QuestionCircleTwoTone } from "@ant-design/icons";
import { LinkContainer } from "react-router-bootstrap";
import Car from "../images/car.png";
import Distance from "../images/distance.png";
import f_trip_date from "../images/first_trip_date.png";
import l_trip_date from "../images/last_trip_date.png";
import Locations from "../images/locations.png";
import Trips from "../images/trips.png";

const { Content, Sider } = Layout;

const HomeScreen = ({ history }) => {
  // const generalInfo = (
  //   <div>
  //     <p>General information of the data source</p>
  //   </div>
  // );

  // const listFleets = (
  //   <div>
  //     <p>List of fleets with their link to their dashboard</p>
  //   </div>
  // );

  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const getFleets = useSelector((state) => state.getFleets);
  const { loadingFleets, fleets } = getFleets;

  const getFleetStats = useSelector((state) => state.getFleetStats);
  const { loadingFleetStats, fleetStats } = getFleetStats;

  useEffect(() => {
    if (!userInfo) history.push("/login");

    dispatch(getFleetInfo());
    dispatch(getStatsFleet());

    // fleets.map((fleet) => {
    //   console.log(fleet.fleet_id);
    //   dispatch(getInfoFleetsById(fleet.fleet_id));
    // });
  }, [dispatch, history, userInfo]);

  return (
    <Layout>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
      >
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={["1"]}
          style={{ height: "100%" }}
        >
          <Menu.Item key="1" icon={<UserOutlined />}>
            Home
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout" style={{ marginLeft: 100 }}>
        <Content style={{ padding: "0 0px", marginTop: 2 }}>
          <div
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {loadingFleetStats ? (
              <Loader />
            ) : fleetStats ? (
              <section className="fdb-block team-5">
                <div className="container">
                  <div className="row text-center justify-content-center">
                    <div className="col-8">
                      {/* {["right"].map((placement) => (
                        <OverlayTrigger
                          key={placement}
                          placement={placement}
                          overlay={
                            <Tooltip id={`tooltip-${placement}`}>
                              General information of the data source
                            </Tooltip>
                          }
                        >
                          <h1>tes</h1>
                        </OverlayTrigger>
                      ))} */}

                      <h1>
                        General Information
                        {/* <Popover placement="rightTop" content={generalInfo}>
                          <QuestionCircleTwoTone />
                        </Popover> */}
                      </h1>
                    </div>
                  </div>

                  <div className="row-5"></div>

                  <div className="row text-center justify-content-center">
                    <div className="col-sm-2 m-sm-auto">
                      <img
                        alt="Drivers"
                        className="img-fluid rounded-circle"
                        src={Car}
                      />
                      <p>Drivers</p>

                      <h3>
                        <strong>{fleetStats.number_of_drivers}</strong>
                      </h3>
                    </div>

                    <div className="col-sm-2 m-sm-auto">
                      <img
                        alt="Trips"
                        className="img-fluid rounded-circle"
                        src={Trips}
                      />
                      <p>Trips</p>

                      <h3>
                        <strong>{fleetStats.number_of_trips}</strong>
                      </h3>
                    </div>

                    <div className="col-sm-2 m-sm-auto">
                      <img
                        alt="Distance"
                        className="img-fluid rounded-circle"
                        src={Distance}
                      />
                      <p>Distance (km)</p>
                      <h3>
                        <strong>{fleetStats.total_distance}</strong>
                      </h3>
                    </div>
                  </div>

                  <div className="row justify-content-center text-center mt-5">
                    <div className="col-sm-2  m-sm-auto">
                      <img
                        alt="Locations"
                        className="img-fluid rounded-circle"
                        src={Locations}
                      />
                      <p>Locations</p>

                      <h3>
                        <strong>{fleetStats.number_of_locations}</strong>
                      </h3>
                    </div>

                    <div className="col-sm-2 m-sm-auto">
                      <img
                        alt="firstripdate"
                        className="img-fluid rounded-circle"
                        src={f_trip_date}
                      />
                      <p>First Trip</p>

                      <h3>
                        <strong>{fleetStats.first_trip_date}</strong>
                      </h3>
                    </div>

                    <div className="col-sm-2 m-sm-auto">
                      <img
                        alt="lastripdate"
                        className="img-fluid rounded-circle"
                        src={l_trip_date}
                      />
                      <p>Last Trip</p>

                      <h3>
                        <strong>{fleetStats.last_trip_date}</strong>
                      </h3>
                    </div>
                  </div>
                </div>
              </section>
            ) : (
              <Loader />
              // <h1>Ups.... Looks like there are some problems.</h1>
            )}
            <section className="fdb-block team-5">
              <div className="container">
                <div className="row text-center justify-content-center">
                  <div className="col-8">
                    <h1>Fleets</h1>
                    {loadingFleets ? (
                      <Loader />
                    ) : fleets ? (
                      <Table
                        striped
                        bordered
                        hover
                        responsive
                        className="table-sm"
                        variant="dark"
                      >
                        <thead>
                          <tr>
                            <th>Fleet</th>
                            <th>Drivers</th>
                            <th>Latest Trip</th>
                          </tr>
                        </thead>
                        <tbody>
                          {fleets.map((fleet) => (
                            <tr key={fleet.fleet_id}>
                              <td>
                                <LinkContainer
                                  to={`/dashboard/fleet/${fleet.fleet_id}`}
                                >
                                  <Button variant="light" className="btn-sm">
                                    {fleet.fleet_id}
                                  </Button>
                                </LinkContainer>
                              </td>

                              <td>{fleet.number_of_drivers}</td>
                              <td>{fleet.last_trip_date}</td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    ) : (
                      <Loader />
                      // <h1>There is no Fleets</h1>
                    )}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default HomeScreen;
