import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { Button, Form } from "react-bootstrap";
import { login } from "../actions/userActions";
// import FormContainer from "../components/FormContainer";

const LoginScreen = ({ history }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    if (userInfo) history.push("/");
  }, [history, userInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(login(username, password));
    localStorage.setItem("username", username);
  };

  return (
    <div className="registration-form">
      <div className="section-title">
        <h2>Login</h2>
      </div>
      <form className="form" onSubmit={submitHandler}>
        <div className="form-icon">
          <span>
            <i className="far fa-user"></i>
          </span>
        </div>
        <div className="form-group">
          <input
            type="username"
            className="form-control item"
            placeholder="Username"
            name="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            className="form-control item"
            placeholder="Password"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <div className="form-group">
          <button
            type="submit"
            className="btn btn-block create-account"
            value="Login"
          >
            Login
          </button>
        </div>
      </form>
    </div>

    // <FormContainer className="registration-form">
    //   <h1>Login</h1>
    //   <Form onSubmit={submitHandler}>
    //     <Form.Group controlId="username" className="form-group">
    //       <Form.Control
    //         type="username"
    //         placeholder="Username"
    //         className="form-control item"
    //         value={username}
    //         onChange={(e) => setUsername(e.target.value)}
    //       ></Form.Control>
    //     </Form.Group>

    //     <Form.Group controlId="password" className="form-group">
    //       <Form.Control
    //         type="password"
    //         placeholder="Password"
    //         className="form-control item"
    //         value={password}
    //         onChange={(e) => setPassword(e.target.value)}
    //       ></Form.Control>
    //     </Form.Group>

    //     <Button type="submit" variant="primary" className="mt-2">
    //       Login
    //     </Button>
    //   </Form>
    // </FormContainer>
  );
};

export default LoginScreen;
