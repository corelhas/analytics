import axios from "axios";
import {
  DRIVER_INFO_REQUEST,
  DRIVER_INFO_SUCCESS,
  DRIVER_INFO_FAIL,
} from "../constants/driverConstants";

export const getDriversInfo = () => async (dispatch, getState) => {
  try {
    dispatch({
      type: DRIVER_INFO_REQUEST,
    });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      `${process.env.REACT_APP_BASE_URL_NG}/v1/drivers`,
      config
    );

    dispatch({
      type: DRIVER_INFO_SUCCESS,
      payload: data,
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    dispatch({
      type: DRIVER_INFO_FAIL,
      payload: message,
    });
  }
};

// export const getDriverSummary = (id) => async (dispatch, getState) => {
//   try {
//     dispatch({ type: DRIVER_SUMMARY_REQUEST });

//     const {
//       userLogin: { userInfo },
//     } = getState();

//     const config = {
//       headers: { Authorization: `Bearer ${userInfo.access_token}` },
//     };

//     const { data } = await axios.get(
//       `${process.env.REACT_APP_ANALYTICS}/drive/v1/summary?driver_id=${10}`
//     );
//   } catch (error) {}
// };
