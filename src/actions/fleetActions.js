import axios from "axios";
import {
  FLEET_INFO_REQUEST,
  FLEET_INFO_SUCCESS,
  FLEET_INFO_FAIL,
  FLEET_STATS_REQUEST,
  FLEET_STATS_SUCCESS,
  FLEET_STATS_FAIL,
  FLEET_BYID_SUCCESS,
  FLEET_BYID_REQUEST,
  FLEET_BYID_FAIL,
} from "../constants/fleetConstants";

export const getFleetInfo = () => async (dispatch, getState) => {
  try {
    dispatch({ type: FLEET_INFO_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      // `${process.env.REACT_APP_ANALYTICS}/info/v1/fleets`,
      "https://api.motion-s.com/analytics/info/v1/fleets",
      config
    );

    dispatch({ type: FLEET_INFO_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: FLEET_INFO_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getStatsFleet = () => async (dispatch, getState) => {
  try {
    dispatch({ type: FLEET_STATS_REQUEST });
    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      "https://api.motion-s.com/analytics/info/v1/stats",
      config
    );

    dispatch({ type: FLEET_STATS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: FLEET_STATS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const getInfoFleetsById = (fleet_id) => async (dispatch, getState) => {
  try {
    dispatch({ type: FLEET_BYID_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      `${process.env.REACT_APP_BASE_URL_NG}/v1/fleets/${fleet_id}/`,
      // `https://api.motion-s.com/platform/v1/fleets/${fleet_id}/`,
      config
    );

    dispatch({ type: FLEET_BYID_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: FLEET_BYID_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
