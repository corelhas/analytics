import axios from "axios";
import {
  FLEET_RISKSUMMARY_REQUEST,
  FLEET_RISKSUMMARY_SUCCESS,
  FLEET_RISKSUMMARY_FAIL,
} from "../../../constants/fleetConstants";

export const getRiskSummary = (fleet_id) => async (dispatch, getState) => {
  try {
    dispatch({ type: FLEET_RISKSUMMARY_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      //   `${process.env.REACT_APP_ANALYTICS_RISK}?fleet_id=${fleet_id}`,

      `https://api.motion-s.com/analytics/risk/v1/summary?fleet_id=${fleet_id}`,
      config
    );

    dispatch({ type: FLEET_RISKSUMMARY_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: FLEET_RISKSUMMARY_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
