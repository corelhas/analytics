import axios from "axios";
import {
  DRIVER_DASHBOARD_REQUEST,
  DRIVER_DASHBOARD_SUCCESS,
  DRIVER_DASHBOARD_FAIL,
} from "../../../constants/driverConstants";

export const getDriverDashboard = (fleet_id) => async (dispatch, getState) => {
  try {
    dispatch({ type: DRIVER_DASHBOARD_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.access_token}`,
      },
    };

    const { data } = await axios.get(
      `${process.env.REACT_APP_ANALYTICS_RISK}/${process.env.REACT_APP_DRIVE_DASHBOARD}?fleet_id=${fleet_id}`,
      config
    );

    dispatch({ type: DRIVER_DASHBOARD_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: DRIVER_DASHBOARD_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
