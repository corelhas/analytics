import axios from "axios";
import {
  DEVICE_INFO_REQUEST,
  DEVICE_INFO_SUCCESS,
  DEVICE_INFO_FAIL,
} from "../constants/devicesConstants";

export const getDevicesInfo = () => async (dispatch, getState) => {
  try {
    dispatch({ type: DEVICE_INFO_REQUEST });

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: { Authorization: `Bearer ${userInfo.access_token}` },
    };

    const { data } = await axios.get(
      `${process.env.REACT_APP_BASE_URL_NG}/v1/devices`,
      config
    );

    // Count the number of devices aggregated by OS
    let arrayCount = [];
    let key = "os";

    data.forEach((x) => {
      // Checking if there is any object in arrayCount
      // which contains the key value (Key = OS )
      if (
        arrayCount.some((val) => {
          return val[key] === x[key];
        })
      ) {
        // If yes! then increase the occurrence by 1
        arrayCount.forEach((k) => {
          if (k[key] === x[key]) {
            k["occurrence"]++;
          }
        });
      } else {
        // If not! Then create a new object initialize
        // it with the present iteration key's value and
        // set the occurrence to 1
        let a = {};
        a[key] = x[key];
        a["occurrence"] = 1;
        arrayCount.push(a);
      }
    });

    dispatch({ type: DEVICE_INFO_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: DEVICE_INFO_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};
