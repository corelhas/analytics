import {
  FLEET_RISKSUMMARY_REQUEST,
  FLEET_RISKSUMMARY_SUCCESS,
  FLEET_RISKSUMMARY_FAIL,
} from "../../../constants/fleetConstants";

export const getFleetRiskSummaryReducer = (
  state = { fleetRisk: {} },
  action
) => {
  const { type, payload } = action;

  switch (type) {
    case FLEET_RISKSUMMARY_REQUEST:
      return { loading: true };
    case FLEET_RISKSUMMARY_SUCCESS:
      return { loading: false, fleetRisk: payload };
    case FLEET_RISKSUMMARY_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};
