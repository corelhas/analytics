import {
  DRIVER_DASHBOARD_REQUEST,
  DRIVER_DASHBOARD_SUCCESS,
  DRIVER_DASHBOARD_FAIL,
} from "../../../constants/driverConstants";

export const getDriverDashboardReducer = (
  state = { loading: true, driverDashboard: {} },
  action
) => {
  const { type, payload } = action;

  switch (type) {
    case DRIVER_DASHBOARD_REQUEST:
      return { loading: true };
    case DRIVER_DASHBOARD_SUCCESS:
      return { loading: false, driverDashboard: payload };
    case DRIVER_DASHBOARD_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};
