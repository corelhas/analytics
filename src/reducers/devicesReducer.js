import {
  DEVICE_INFO_REQUEST,
  DEVICE_INFO_SUCCESS,
  DEVICE_INFO_FAIL,
} from "../constants/devicesConstants";

export const getDeviceReducer = (state = { devices: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case DEVICE_INFO_REQUEST:
      return { loading: true };
    case DEVICE_INFO_SUCCESS:
      return { loading: false, devices: payload };
    case DEVICE_INFO_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};
