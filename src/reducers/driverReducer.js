import {
  DRIVER_INFO_FAIL,
  DRIVER_INFO_REQUEST,
  DRIVER_INFO_SUCCESS,
} from "../constants/driverConstants";

export const getDriversReducer = (state = { drivers: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case DRIVER_INFO_REQUEST:
      return { loading: true };
    case DRIVER_INFO_SUCCESS:
      return { loading: false, drivers: payload };
    case DRIVER_INFO_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};
