import {
  FLEET_INFO_REQUEST,
  FLEET_INFO_SUCCESS,
  FLEET_INFO_FAIL,
  FLEET_STATS_REQUEST,
  FLEET_STATS_SUCCESS,
  FLEET_STATS_FAIL,
  FLEET_BYID_REQUEST,
  FLEET_BYID_SUCCESS,
  FLEET_BYID_FAIL,
} from "../constants/fleetConstants";

export const getFleetsReducer = (state = { fleets: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FLEET_INFO_REQUEST:
      return { loading: true };
    case FLEET_INFO_SUCCESS:
      return { loading: false, fleets: payload };
    case FLEET_INFO_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};

export const getFleetStatsReducer = (
  state = { loading: true, fleets: [] },
  action
) => {
  const { type, payload } = action;

  switch (type) {
    case FLEET_STATS_REQUEST:
      return { loading: true };
    case FLEET_STATS_SUCCESS:
      return { loading: false, fleetStats: payload };
    case FLEET_STATS_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};

export const getFleetByIdReducer = (state = { fleetInfo: {} }, action) => {
  const { type, payload } = action;

  switch (type) {
    case FLEET_BYID_REQUEST:
      return { loading: true };
    case FLEET_BYID_SUCCESS:
      return { loading: false, fleetInfo: payload };
    case FLEET_BYID_FAIL:
      return { loading: false, error: payload };
    default:
      return state;
  }
};
